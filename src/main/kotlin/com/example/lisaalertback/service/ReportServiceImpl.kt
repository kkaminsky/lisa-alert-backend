package com.example.lisaalertback.service

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream

@Service
class ReportServiceImpl : ReportService {

    override fun openReport(fileName: String): Workbook {
        return WorkbookFactory.create(File(fileName))
    }

    override fun setResult(wb: Workbook, outFile: String,  process: (r: Row) -> Unit) {
        val sheet = wb.getSheet("Sheet1")
        sheet.rowIterator().forEachRemaining { row ->
            process(row)
        }

        FileOutputStream(outFile).use {
            wb.write(it)
            wb.close()
        }

    }
}
