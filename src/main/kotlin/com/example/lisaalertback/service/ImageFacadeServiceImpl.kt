package com.example.lisaalertback.service

import ai.djl.modality.cv.ImageFactory
import com.example.lisaalertback.repository.TagRepository
import com.example.lisaalertback.repository.TagToImageRepository
import com.example.lisaalertback.repository.TargetTagRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.collections.HashMap


@Service
class ImageFacadeServiceImpl(
    private val minioService: MinioService,
    private val imageService: ImageService,
    private val tagToImageRepository: TagToImageRepository,
    private val predictService: PredictService,
    private val targetTagRepository: TargetTagRepository,
    private val tagRepository: TagRepository,
    private val excelService: ExcelService
) : ImageFacadeService {

    companion object{
        val map: HashMap<String, HashMap<String, Int>> = hashMapOf()
    }

    override fun predictImage(img: File): List<String> {
        val classes = img.inputStream().use {
            predictService.predictClasses(it)
        }

        return classes.map { it.className }.toSet().toList()
    }


    override fun uploadImage(img: File, fileName: String) {
        val path = minioService.uploadFile(img.inputStream(),fileName)

        val image = imageService.saveImage(path)

        val classes = img.inputStream().use {
            predictService.predictClasses(it)
        }


        val classNames = classes.map { it.className }.toSet()

        classNames.forEach { kClassName ->
            val targetHashMap: MutableMap<String, Int> = (map[kClassName])?: run {
                map[kClassName] = hashMapOf()
                map[kClassName]!!
            }

                val targetTags = excelService.getTargetTagsNamesFromExcelForFile(fileName)

                targetTags.forEach { targetTag ->
                    val targetField = targetHashMap[targetTag]
                    if (targetField == null)
                        targetHashMap[targetTag] = 1
                    else
                        targetHashMap[targetTag] = targetField + 1
                }
            }


        if (classes.isNotEmpty()){
            println(map)
            imageService.saveTagsForImage(classes, image)
        }
    }

    override fun getByTargetTags(tagNames: List<String>): List<String> {
        val results = mutableListOf<String>()

        for (targetTag in tagNames){
            if (targetTag == "Кинолог"){
                val tags = tagRepository.findByTargetTagName(targetTag)
                val images = tagToImageRepository.findByTagTargetTagName(targetTag)
                    .groupBy { it.image }

                val res = images.filter { it.value.map { it.tag.name }.containsAll(tags.map { it.name }) }
                results.addAll(res.keys.map { it.path })
            }
            else {
                results.addAll(tagToImageRepository.findByTag_TargetTag_Name(targetTag).map {
                    it.image.path
                }.toSet().toList())
            }
        }
        return results
    }

    override fun uploadWithRec(img: MultipartFile): File {
        val fileNewFile = File.createTempFile("prefix-", "-suffix")
        FileOutputStream(fileNewFile).use {
            img.transferTo(fileNewFile)
        }

        val classes = fileNewFile.inputStream().use {
            predictService.predictClassesCore(it)
        }

        val image = fileNewFile.inputStream().use {
            ImageFactory.getInstance().fromInputStream(it)
        }

        val outputDir: Path = Paths.get("build/output")
        Files.createDirectories(outputDir)

        classes.forEach {
            image.drawBoundingBoxes(it)
        }

        val outpuFile = File.createTempFile("prefix-", "-suffix")
        image.save(outpuFile.outputStream(), "png")
        return outpuFile
    }


    @Transactional(readOnly = true)
    override fun getByTags(tagNames: List<String>): List<String> {
        return tagToImageRepository.findByTagNameIn(tagNames).map {
            it.image.path
        }.toSet().toList()
    }
}