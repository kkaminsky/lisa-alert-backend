package com.example.lisaalertback.service

import ai.djl.modality.cv.output.DetectedObjects
import com.example.lisaalertback.config.MinioConfig
import com.example.lisaalertback.entity.Image
import com.example.lisaalertback.entity.Tag
import com.example.lisaalertback.entity.TagToImage
import com.example.lisaalertback.repository.ImageRepository
import com.example.lisaalertback.repository.TagRepository
import com.example.lisaalertback.repository.TagToImageRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ImageServiceImpl(
    private val imageRepository: ImageRepository,
    private val tagRepository: TagRepository,
    private val tagToImageRepository: TagToImageRepository,
    private val minioConfig: MinioConfig
) : ImageService {
    @Transactional
    override fun saveTagsForImage(classes: List<DetectedObjects.DetectedObject>, image: Image) {
        val classNames = classes.map { it.className.lowercase() }.toSet().toList() //TODO: already unique?
        val existedTags = tagRepository.findByNameIn(classNames)
        val tagsForSave = classNames.filter { it !in existedTags.map { it.name } }
        val tags = tagsForSave.map {
            Tag(name = it,null)
        }
        tagRepository.saveAll(tags)

        val tagToImage = existedTags.plus(tags).map { tag ->
            val targetClass = classes.find { it.className.lowercase() == tag.name }
                ?: throw Exception("Model class not found")

            val points = targetClass.boundingBox?.path
            val xmin = points?.minByOrNull { point -> point.x }?.x
            val ymin = points?.minByOrNull { point -> point.y }?.y
            val xmax = points?.maxByOrNull { point -> point.x }?.x
            val ymax = points?.maxByOrNull { point -> point.y }?.y
            TagToImage(
                tag = tag,
                image = image,
                prediction = targetClass.probability,
                cordxmin = xmin,
                cordymin = ymin,
                cordxmax = xmax,
                cordymax = ymax
            )
        }
        tagToImageRepository.saveAll(tagToImage)
    }

    @Transactional
    override fun saveImage(minioPath: String): Image {
        val image = Image(minioPath)
        return imageRepository.save(image)
    }

    @Transactional(readOnly = true)
    override fun getImageByName(fileName: String): Image? {
        return imageRepository.findByPath(minioConfig.defaultBaseFolder + "/" + fileName)
    }
}