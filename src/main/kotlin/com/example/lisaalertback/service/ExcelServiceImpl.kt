package com.example.lisaalertback.service

import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.stereotype.Service
import java.io.File

@Service
class ExcelServiceImpl : ExcelService {

    val dayMap = hashMapOf(
        1 to "День",
        2 to "Ночь",
        3 to "Рассвет/закат"
    )

    val seasonMap = hashMapOf(
        1 to "Зима",
        2 to "Весна",
        3 to "Лето",
        4 to "Осень"
    )

    val locationMap = hashMapOf(
        1 to "Лес",
        2 to "Город"
    )

    val imageNameToCellNumber = mutableMapOf<String,Int>()

    lateinit var sheet: Sheet
    init {

        val workBook = WorkbookFactory.create(File("/home/konstantin/opt/projects/vott-test/ИИ_фотобанк_для_поискового_отряда/razm.xlsx"))

        sheet = workBook.getSheet("Sheet1")

        sheet.forEachIndexed { index, row ->
            imageNameToCellNumber.put(row.getCell(0).stringCellValue, index)
        }
    }

    override fun getValueFromLastColumn(fileName: String): String {
        val row = sheet.getRow(imageNameToCellNumber[fileName]!!)
        return row.getCell(12).stringCellValue
    }

    override fun getTargetTagsNamesFromExcelForFile(fileName: String): List<String> {

        val row = sheet.getRow(imageNameToCellNumber[fileName]!!)

        val minColIx = row.firstCellNum + 1
        val maxColIx = row.lastCellNum - 1

        val result = mutableListOf<String>()

        for (colIx in minColIx until maxColIx) {
            val data = row.getCell(colIx).numericCellValue.toInt()
            if (colIx==1){
                if (dayMap.get(data) != null)
                    result.add(dayMap.get(data)!!)
            }

            if (colIx==2){
                if (seasonMap.get(data) != null)
                    result.add(seasonMap.get(data)!!)
            }
            if (colIx==3){
                if (locationMap.get(data) != null)
                    result.add(locationMap.get(data)!!)
            }
            if (colIx>3){
                if (data==1){
                    result.add(sheet.first().getCell(colIx).stringCellValue)
                }
            }
        }
        return result
    }
}