package com.example.lisaalertback.service

import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.InputStream

interface ImageFacadeService {
    fun predictImage(img: File): List<String>
    fun uploadImage(img: File, fileName: String)
    fun uploadWithRec(img: MultipartFile): File
    fun getByTags(tagNames: List<String>): List<String>
    fun getByTargetTags(tagNames: List<String>): List<String>
}