package com.example.lisaalertback.service

import ai.djl.modality.cv.Image
import ai.djl.modality.cv.output.BoundingBox
import ai.djl.modality.cv.output.DetectedObjects
import ai.djl.modality.cv.output.Rectangle
import ai.djl.modality.cv.util.NDImageUtils
import ai.djl.ndarray.NDArray
import ai.djl.ndarray.NDList
import ai.djl.ndarray.types.DataType
import ai.djl.translate.NoBatchifyTranslator
import ai.djl.translate.TranslatorContext
import ai.djl.util.JsonUtils
import java.io.BufferedInputStream
import java.io.IOException
import java.net.URL
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.ConcurrentHashMap

private class Item2 {
    var id = 0
    var name: String? = null
}

@Throws(IOException::class)
fun lizaLoadSynset(): Map<Int, String>? {
    val synsetUrl = URL(
            "file:///home/konstantin/opt/projects/lisa-alert-back/labelmap.pbtxt")
    val map: MutableMap<Int, String> = ConcurrentHashMap()
    var maxId = 0
    BufferedInputStream(synsetUrl.openStream()).use { `is` ->
        Scanner(`is`, StandardCharsets.UTF_8.name()).use { scanner ->
            scanner.useDelimiter("item ")
            while (scanner.hasNext()) {
                var content: String = scanner.next()
                content = content.replace("(\"|\\d)\\n\\s".toRegex(), "$1,")
                val item: Item2 = JsonUtils.GSON.fromJson(content, Item2::class.java)
                map[item.id] = item.name ?: ""
                if (item.id > maxId) {
                    maxId = item.id
                }
            }
        }
    }
    return map.toMap()
}

class LizaTranslator internal constructor() : NoBatchifyTranslator<Image?, DetectedObjects?> {
    private var classes: Map<Int, String>? = null
    private val maxBoxes = 10
    private val threshold = 0.88f

    /** {@inheritDoc}  */
    override fun processInput(ctx: TranslatorContext?, input: Image?): NDList? {
        // input to tf object-detection models is a list of tensors, hence NDList
        var array: NDArray = input!!.toNDArray(ctx!!.ndManager, Image.Flag.COLOR)
        // optionally resize the image for faster processing
        array = NDImageUtils.resize(array, 224)
        // tf object-detection models expect 8 bit unsigned integer tensor
        array = array.toType(DataType.UINT8, true)
        array = array.expandDims(0) // tf object-detection models expect a 4 dimensional input
        return NDList(array)
    }

    /** {@inheritDoc}  */
    @Throws(IOException::class)
    override fun prepare(ctx: TranslatorContext) {
        if (classes == null) {
            classes = lizaLoadSynset()
        }
    }

    /** {@inheritDoc}  */
    override fun processOutput(ctx: TranslatorContext, list: NDList): DetectedObjects {
        // output of tf object-detection models is a list of tensors, hence NDList in djl
        // output NDArray order in the list are not guaranteed
        var classIds: IntArray? = null
        var probabilities: FloatArray? = null
        var boundingBoxes: NDArray? = null
        for (array in list) {
            if ("detection_boxes" == array.name) {
                boundingBoxes = array[0]
            } else if ("detection_scores" == array.name) {
                probabilities = array[0].toFloatArray()
            } else if ("detection_classes" == array.name) {
                // class id is between 1 - number of classes
                classIds = array[0].toType(DataType.INT32, true).toIntArray()
            }
        }
        Objects.requireNonNull(classIds)
        Objects.requireNonNull(probabilities)
        Objects.requireNonNull(boundingBoxes)
        val retNames: MutableList<String> = ArrayList()
        val retProbs: MutableList<Double> = ArrayList()
        val retBB: MutableList<BoundingBox> = ArrayList()

        // result are already sorted
        for (i in 0 until Math.min(classIds!!.size, maxBoxes)) {
            val classId = classIds[i]
            val probability = probabilities!![i].toDouble()
            // classId starts from 1, -1 means background
            if (classId > 0 && probability > threshold) {
                val className = classes!![classId] ?: "#$classId"
                val box = boundingBoxes!![i.toLong()].toFloatArray()
                val yMin = box[0]
                val xMin = box[1]
                val yMax = box[2]
                val xMax = box[3]
                val rect = Rectangle(xMin.toDouble(),
                        yMin.toDouble(),
                        xMax.toDouble() - xMin.toDouble(),
                        yMax.toDouble() - yMin.toDouble())
                retNames.add(className)
                retProbs.add(probability)
                retBB.add(rect)
            }
        }
        return DetectedObjects(retNames, retProbs, retBB)
    }
}