package com.example.lisaalertback.service

import org.springframework.web.multipart.MultipartFile

interface TagService {
    fun getTagsByImage(image: MultipartFile): List<String>
}