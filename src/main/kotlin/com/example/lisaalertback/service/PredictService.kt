package com.example.lisaalertback.service

import ai.djl.modality.cv.output.DetectedObjects
import java.io.InputStream

interface PredictService {
    fun predictClasses(file: InputStream): List<DetectedObjects.DetectedObject>

    fun predictClassesCore(file: InputStream): List<DetectedObjects>

}