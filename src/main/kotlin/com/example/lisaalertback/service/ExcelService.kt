package com.example.lisaalertback.service

interface ExcelService {
    fun getTargetTagsNamesFromExcelForFile(fileName: String):List<String>
    fun getValueFromLastColumn(fileName: String): String
}