package com.example.lisaalertback.service

import com.example.lisaalertback.repository.TagRepository
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class TagServiceImpl(
    private val predictService: PredictService,
    private val tagRepository: TagRepository,
) : TagService {

    override fun getTagsByImage(image: MultipartFile): List<String> {

        return tagRepository.findByTargetTagNameIn(predictService.predictClasses(image.inputStream)
            .map { it.className }).map { it.targetTag!!.name }
    }
}