package com.example.lisaalertback.service

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Workbook

interface ReportService {
    fun openReport(fileName: String): Workbook
    fun setResult(wb: Workbook, outFile: String,  process: (r: Row) -> Unit)
}