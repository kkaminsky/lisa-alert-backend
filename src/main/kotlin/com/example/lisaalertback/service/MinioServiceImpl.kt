package com.example.lisaalertback.service

import com.example.lisaalertback.config.MinioConfig
import io.minio.GetObjectArgs
import io.minio.MinioClient
import io.minio.PutObjectArgs
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.buffer.DefaultDataBuffer
import org.springframework.core.io.buffer.DefaultDataBufferFactory
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.*


@Service
class MinioServiceImpl(
    private val minioClient: MinioClient,
    private val minioConfig: MinioConfig
) : MinioService {


    override fun uploadFile(file: InputStream, fileName: String): String {
        val args = PutObjectArgs.builder()
            .bucket(minioConfig.defaultBucketName)
            .`object`(minioConfig.defaultBaseFolder + "/" + fileName)
            .stream(file, file.available().toLong(), -1)
            .build()

        val response = minioClient.putObject(args)
        return response.`object`()

    }

    override fun getFile(filePath: String): ByteArray {
       val args = GetObjectArgs.builder()
           .bucket(minioConfig.defaultBucketName)
           .`object`(filePath)
           .build()
       val res = minioClient.getObject(args)

       return res.readAllBytes()
    }
}