package com.example.lisaalertback.service

import ai.djl.modality.cv.output.DetectedObjects
import com.example.lisaalertback.entity.Image

interface ImageService {
    fun saveTagsForImage(classes: List<DetectedObjects.DetectedObject>, image: Image)
    fun saveImage(minioPath: String): Image
    fun getImageByName(fileName: String): Image?
}