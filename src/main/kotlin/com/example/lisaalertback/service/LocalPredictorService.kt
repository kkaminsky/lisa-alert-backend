package com.example.lisaalertback.service

interface LocalPredictorService {
    fun predictAndSaveImagesFromLocalPath(localPath: String)
    fun fScore(localPath: String, outExcelFile: String)
}