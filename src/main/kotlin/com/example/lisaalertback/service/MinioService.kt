package com.example.lisaalertback.service

import org.springframework.core.io.buffer.DefaultDataBuffer
import org.springframework.http.ResponseEntity
import org.springframework.web.multipart.MultipartFile
import java.io.InputStream

interface MinioService {
    fun uploadFile(file: InputStream, fileName: String): String
    fun getFile(filePath: String): ByteArray
}