package com.example.lisaalertback.service

import com.example.lisaalertback.repository.TagRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream


@Service
class LocalPredictorServiceImpl(
        private val imageFacadeService: ImageFacadeService,
        private val imageService: ImageService,
        private val tagRepository: TagRepository,
        private val excelService: ExcelService,
        private val reportService: ReportService,
        private val objectMapper: ObjectMapper
) : LocalPredictorService {
    val groupMap = hashMapOf<String, Boolean>()

    override fun predictAndSaveImagesFromLocalPath(localPath: String) {
        File(localPath).listFiles().forEachIndexed { index, file ->
            if (index % 1000 == 0) {
                File("/home/konstantin/results_${index}.json").writeBytes(objectMapper.writeValueAsString(ImageFacadeServiceImpl.map).toByteArray())
            }
            println(index)
            /*val group = excelService.getValueFromLastColumn(file.name)
            if (groupMap[group] != null) return@forEachIndexed
            groupMap[group] = true*/
            imageFacadeService.uploadImage(file, file.name)
        }
        File("/home/konstantin/results.json").writeBytes(objectMapper.writeValueAsString(ImageFacadeServiceImpl.map).toByteArray())
    }


    override fun fScore(localPath: String, outExcelFile: String) {
        val resource: InputStream = ClassPathResource(
                "templ.xlsx").inputStream

        val wb = WorkbookFactory.create(resource)
        val sheet = wb.createSheet("Result")

        File(localPath).listFiles().forEachIndexed { index, file ->
            val tags = imageFacadeService.predictImage(file)

            println(tags)

            val tagEntities = tagRepository.findByNameIn(tags).map { it.targetTag }.filter { it?.name != null }

            val dayMap = hashMapOf(
                1 to "День",
                2 to "Ночь",
                3 to "Рассвет/закат"
            )

            val seasonMap = hashMapOf(
                1 to "Зима",
                2 to "Весна",
                3 to "Лето",
                4 to "Осень"
            )

            val locationMap = hashMapOf(
                1 to "Лес",
                2 to "Город"
            )

            val fileName = file.name
            var vremaSutok = 0

            dayMap.forEach {  i, s ->
                tagEntities.firstOrNull{
                    it!!.name == s
                } ?: return@forEach
                vremaSutok = i
            }

            var vremaGoda = 0
            seasonMap.forEach {  i, s ->
                tagEntities.firstOrNull{
                    it!!.name == s
                } ?: return@forEach
                vremaGoda = i
            }

            var mestnost = 0
            locationMap.forEach {  i, s ->
                tagEntities.firstOrNull{
                    it!!.name == s
                } ?: return@forEach
                mestnost = i
            }

            val avia = tagEntities.firstOrNull { it!!.name == "Авиа" }?.let { 1 } ?:0
            val auto = tagEntities.firstOrNull { it!!.name == "Автомобили" }?.let { 1 } ?:0
            val bpla = tagEntities.firstOrNull { it!!.name == "БПЛА" }?.let { 1 } ?:0
            val vodolaz = tagEntities.firstOrNull { it!!.name == "Водолаз" }?.let { 1 } ?:0
            val kinolog = tagEntities.firstOrNull { it!!.name == "Кинолог" }?.let { 1 } ?:0
            val koni = tagEntities.firstOrNull { it!!.name == "Кони" }?.let { 1 } ?:0
            val objytia = tagEntities.firstOrNull { it!!.name == "Объятия" }?.let { 1 } ?:0
            val sherp = tagEntities.firstOrNull { it!!.name == "Шерп" }?.let { 1 } ?:0


            val row = sheet.getRow(index + 1) ?: sheet.createRow(index + 1)

            // имя
            val cell = row.getCell(0) ?: row.createCell(0)
            cell.setCellValue(fileName)

            // Время суток
            val cell0 = row.getCell(1) ?: row.createCell(1)
            cell0.setCellValue(vremaSutok.toDouble())

            // Время года
            val cell1 = row.getCell(2) ?: row.createCell(2)
            cell1.setCellValue(vremaGoda.toDouble())

            // Местность
            val cell2 = row.getCell(3) ?: row.createCell(3)
            cell2.setCellValue(mestnost.toDouble())

            // Авиа
            val cell3 = row.getCell(4) ?: row.createCell(4)
            cell3.setCellValue(avia.toDouble())

            // Автомобили
            val cell4 = row.getCell(5) ?: row.createCell(5)
            cell4.setCellValue(auto.toDouble())

            // БПЛА
            val cell5 = row.getCell(6) ?: row.createCell(6)
            cell5.setCellValue(bpla.toDouble())

            // Водолаз
            val cell6 = row.getCell(7) ?: row.createCell(7)
            cell6.setCellValue(vodolaz.toDouble())

            // Кинолог
            val cell7 = row.getCell(8) ?: row.createCell(8)
            cell7.setCellValue(kinolog.toDouble())

            // Кони
            val cell8 = row.getCell(9) ?: row.createCell(9)
            cell8.setCellValue(koni.toDouble())

            // Объятия
            val cell9 = row.getCell(10) ?: row.createCell(10)
            cell9.setCellValue(objytia.toDouble())

            // Шерп
            val cell10 = row.getCell(11) ?: row.createCell(11)
            cell10.setCellValue(sherp.toDouble())
        }

        FileOutputStream(outExcelFile).use {
            wb.write(it)
            wb.close()
        }

    }
}