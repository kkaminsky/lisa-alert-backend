package com.example.lisaalertback.service.models

import ai.djl.Application
import ai.djl.modality.Classifications
import ai.djl.modality.cv.Image
import ai.djl.modality.cv.ImageFactory
import ai.djl.modality.cv.output.DetectedObjects
import ai.djl.repository.zoo.Criteria
import ai.djl.training.util.ProgressBar
import org.springframework.stereotype.Service
import java.io.InputStream


@Service
class ActionRecognitionImpl : ModelService {

    companion object {

        private var criteria = Criteria.builder()
                .optApplication(Application.CV.ACTION_RECOGNITION)
                .setTypes(Image::class.java, Classifications::class.java)
                .optFilter("backbone", "vgg16")
                .optArgument("threshold", 0.8)
                .optProgress(ProgressBar())
                .build()

        private val model = criteria.loadModel()
        val predictor = model.newPredictor()
    }

    override fun predict(file: InputStream): List<DetectedObjects.DetectedObject> {
        val img = ImageFactory.getInstance().fromInputStream(file)
        val detection = predictor.predict(img)
        val best  = detection.best<Classifications.Classification>()
        return listOf(DetectedObjects.DetectedObject(best.className,best.probability,null))
    }

    override fun predictCore(file: InputStream): DetectedObjects? {
        return null
    }
}