package com.example.lisaalertback.service.models

import ai.djl.modality.cv.output.DetectedObjects
import java.io.InputStream

interface ModelService {
    fun predict(file: InputStream): List<DetectedObjects.DetectedObject>

    fun predictCore(file: InputStream): DetectedObjects?
}