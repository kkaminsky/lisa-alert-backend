package com.example.lisaalertback.service.models

import ai.djl.Application
import ai.djl.modality.cv.Image
import ai.djl.modality.cv.ImageFactory
import ai.djl.modality.cv.output.DetectedObjects
import ai.djl.repository.zoo.Criteria
import ai.djl.training.util.ProgressBar
import org.springframework.stereotype.Service
import java.io.InputStream

@Service
class ResnetModelServiceImpl : ModelService {
    companion object {


        private val criteria = Criteria.builder()
            .optApplication(Application.CV.OBJECT_DETECTION)
            .setTypes(Image::class.java, DetectedObjects::class.java)
            .optFilter("backbone", "resnet50")
            .optEngine("MXNet")
            .optProgress(ProgressBar())
            .build()

        private val model = criteria.loadModel()
        val predictor = model.newPredictor()
    }

    override fun predict(file: InputStream): List<DetectedObjects.DetectedObject> {
        val img = ImageFactory.getInstance().fromInputStream(file)
        val detection = predictor.predict(img)
        return detection.items()
    }

    override fun predictCore(file: InputStream): DetectedObjects? {
        val img = ImageFactory.getInstance().fromInputStream(file)
        val detection = predictor.predict(img)
        return detection
    }
}