package com.example.lisaalertback.service

import ai.djl.modality.cv.output.DetectedObjects
import com.example.lisaalertback.service.models.ModelService
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream

@Service
class PredictServiceImpl(
    private val models: List<ModelService>
) : PredictService {
    override fun predictClasses(file: InputStream): List<DetectedObjects.DetectedObject> {
        val fileNewFile = File.createTempFile("prefix-", "-suffix")
        FileOutputStream(fileNewFile).use {
            file.transferTo(it)
        }
        fileNewFile.deleteOnExit()
        return try {
             models.flatMap { model ->
                fileNewFile.inputStream().use {
                    model.predict(it)
                }
            }.toSet().toList().filter { it.probability > 0.8 }
        } catch (e: Throwable){
            println(e)
            emptyList()
        }
    }

    override fun predictClassesCore(file: InputStream): List<DetectedObjects> {
        val fileNewFile = File.createTempFile("prefix-", "-suffix")
        FileOutputStream(fileNewFile).use {
            file.transferTo(it)
        }
        return try {
            models.mapNotNull { model ->
                fileNewFile.inputStream().use {
                    model.predictCore(it)
                }
            }
        } catch (e: Throwable){
            println(e)
            emptyList()
        }
    }
}