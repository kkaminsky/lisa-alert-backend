package com.example.lisaalertback.config

import io.minio.MinioClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
@ConfigurationProperties(prefix = "minio")
class MinioConfig {

    var accessKey: String = "minioadmin"

    var accessSecret: String = "minioadmin"

    var minioUrl: String = "http://172.29.29.20:9000"

    var defaultBucketName: String = "logisticsphoto"

    var defaultBaseFolder: String = "lisaalert"


    @Bean
    fun generateMinioClient(): MinioClient {
        return try {
            MinioClient.builder()
                .credentials(accessKey, accessSecret)
                .endpoint(minioUrl)
                .build()
        } catch (e: Exception) {
            throw RuntimeException(e.message)
        }
    }
}