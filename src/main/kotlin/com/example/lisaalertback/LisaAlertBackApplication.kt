package com.example.lisaalertback

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LisaAlertBackApplication

fun main(args: Array<String>) {
    runApplication<LisaAlertBackApplication>(*args)
}
