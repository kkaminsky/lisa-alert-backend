package com.example.lisaalertback.controller

import com.example.lisaalertback.entity.TargetTag
import com.example.lisaalertback.repository.TagRepository
import com.example.lisaalertback.repository.TargetTagRepository
import com.example.lisaalertback.service.TagService
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping(value = ["/api/tag"])
class TagController(
    private val tagRepository: TagRepository,
    private val tagService: TagService,
    private val targetTagRepository: TargetTagRepository
) {

    @GetMapping("/all")
    fun getAllTags(): List<String>{
        return tagRepository.findAll().map { it.name }
    }

    @GetMapping("/target/all")
    fun getAllTargetTags(): List<String>{
        return targetTagRepository.findAll().map { it.name }
    }

    @PostMapping("/by/image")
    fun getTagsByImage(@RequestBody file: MultipartFile): Set<String>{

        return tagService.getTagsByImage(file).toSet()
    }

    @PostMapping("/create/targets")
    fun createTargets(){
        targetTagRepository.saveAll(
            listOf("Зима","Лето","Весна","Осень","День","Ночь","Лес","Город","Авиа","Автомобили","БПЛА","Водолаз","Кинолог","Кони","Объятия","Шерп")
                .map {
                    TargetTag(it)
                }
        )
    }
}