package com.example.lisaalertback.controller

import com.example.lisaalertback.service.LocalPredictorService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/api/predicator"])
class PredicatorController(
        private val localPredictorService: LocalPredictorService
) {

    @PostMapping("/start")
    fun startPredicator(@RequestBody dto: StartPredictDto) {
        localPredictorService.predictAndSaveImagesFromLocalPath(dto.path)
    }

    @PostMapping("/createFile")
    fun createFile(@RequestBody dto: CreateFileDto) {
        localPredictorService.fScore(dto.localPath, dto.outExcelFile)
    }

    data class CreateFileDto(
            val localPath: String,
            val outExcelFile: String
    )

    data class StartPredictDto(
            val path: String
    )
}