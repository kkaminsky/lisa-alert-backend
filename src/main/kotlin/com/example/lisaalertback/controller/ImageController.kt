package com.example.lisaalertback.controller

import com.example.lisaalertback.service.ImageFacadeService
import com.example.lisaalertback.service.MinioService
import com.example.lisaalertback.utils.InputStreamCustomResource
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.io.File

@RestController
@RequestMapping(value = ["/api/image"])
class ImageController(
        private val imageFacadeService: ImageFacadeService,
        private val minioService: MinioService
) {

    @PostMapping("/upload")
    fun uploadImage(@RequestBody file: MultipartFile) {
        val fileNewFile = File.createTempFile("prefix-2", "2-suffix")

        file.transferTo(fileNewFile)

        return imageFacadeService.uploadImage(fileNewFile, file.originalFilename!!)
    }

    @PostMapping("/uploadWithRec")
    fun uploadWithRec(@RequestBody file: MultipartFile): ResponseEntity<InputStreamCustomResource> {
        val contentDisposition =
                ContentDisposition.builder("attachment").filename("img.png", Charsets.UTF_8).build()

        val headers = HttpHeaders()
        headers.contentDisposition = contentDisposition
        headers.set(HttpHeaders.CONTENT_TYPE, "image/png")

        val file = imageFacadeService.uploadWithRec(file)

        return ResponseEntity(
                InputStreamCustomResource(
                        file.inputStream(), "img.png"
                ), headers, 200
        )
    }

    @PostMapping("/download", produces = [MediaType.IMAGE_JPEG_VALUE])
    fun getImage(@RequestBody dto: GetFile): ByteArray {
        return minioService.getFile(dto.path)
    }

    @PostMapping("/by/tags")
    fun getImageByTags(@RequestBody dto: GetImageByTags): List<String> {
        return imageFacadeService.getByTags(dto.tags)
    }

    @PostMapping("/by/target/tags")
    fun getImageByTargetTags(@RequestBody dto: GetImageByTags): List<String> {
        return imageFacadeService.getByTargetTags(dto.tags)
    }


    data class GetFile(
            val path: String
    )

    data class GetImageByTags(
            val tags: List<String>
    )
}