package com.example.lisaalertback.entity

import com.example.lisaalertback.entity.base.BaseUUIDEntity
import javax.persistence.*

@Entity
@Access(value= AccessType.FIELD)
data class TagToImage(
    @field:ManyToOne
    @field:JoinColumn(name = "tag_id", nullable = false)
    var tag: Tag,

    @field:ManyToOne
    @field:JoinColumn(name = "image_id", nullable = false)
    var image: Image,

    val prediction: Double,

    val cordxmin: Double?,

    val cordymin: Double?,

    val cordxmax: Double?,

    val cordymax: Double?
): BaseUUIDEntity()