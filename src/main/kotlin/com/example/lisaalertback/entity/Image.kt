package com.example.lisaalertback.entity

import com.example.lisaalertback.entity.base.BaseUUIDEntity
import javax.persistence.Access
import javax.persistence.AccessType
import javax.persistence.Entity

@Entity
@Access(value= AccessType.FIELD)
data class Image(
    var path: String
): BaseUUIDEntity()