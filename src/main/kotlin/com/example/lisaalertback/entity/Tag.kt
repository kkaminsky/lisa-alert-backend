package com.example.lisaalertback.entity

import com.example.lisaalertback.entity.base.BaseUUIDEntity
import javax.persistence.*

@Entity
@Access(value= AccessType.FIELD)
data class Tag(
    var name: String,

    @field:ManyToOne
    @field:JoinColumn(name = "target_tag_id", nullable = true)
    var targetTag: TargetTag?,
): BaseUUIDEntity()