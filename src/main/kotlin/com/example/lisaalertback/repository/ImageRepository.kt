package com.example.lisaalertback.repository

import com.example.lisaalertback.entity.Image
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface ImageRepository : PagingAndSortingRepository<Image, UUID>{
    fun findByPath(string: String): Image?
}