package com.example.lisaalertback.repository

import com.example.lisaalertback.entity.Tag
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface TagRepository : PagingAndSortingRepository<Tag, UUID> {
    fun findByNameIn(names: List<String>): List<Tag>
    fun findByTargetTagName(name: String): List<Tag>
    fun findByTargetTagNameIn(names: List<String>): List<Tag>
}