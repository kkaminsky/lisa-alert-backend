package com.example.lisaalertback.repository

import com.example.lisaalertback.entity.TargetTag
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface TargetTagRepository : PagingAndSortingRepository<TargetTag, UUID>{
    fun findByNameIn(names: List<String>): List<TargetTag>
    fun findByName(name: String): TargetTag?
}