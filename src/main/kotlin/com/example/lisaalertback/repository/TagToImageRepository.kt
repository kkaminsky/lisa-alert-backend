package com.example.lisaalertback.repository

import com.example.lisaalertback.entity.TagToImage
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface TagToImageRepository : PagingAndSortingRepository<TagToImage, UUID> {

    fun findByTagNameIn(tagNames: List<String>): List<TagToImage>
    fun findByTagTargetTagNameIn(tagNames: List<String>): List<TagToImage>
    fun findByTagTargetTagName(tagName: String): List<TagToImage>
    fun findByTag_TargetTag_Name(tagName: String): List<TagToImage>
}